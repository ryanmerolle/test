# anybadge Test

[![Latest Version](https://gitlab.org/ryanmerolle/test/-/jobs/artifacts/main/raw/badges/version.svg?job=create_badge)](https://gitlab.org/ryanmerolle/test/-/tags)
[![Commits](https://gitlab.org/ryanmerolle/test/-/jobs/artifacts/main/raw/badges/commits.svg?job=create_badge)](https://gitlab.com/ryanmerolle/test/-/commits/main)

[![Latest Version](https://img.shields.io/static/v1?label=latest&message=v1.0.0&color=informational&style=plastic&logo=docker&logoColor=white)](https://gitlab.com/ryanmerolle/test/-/releases/v1.0.0)
